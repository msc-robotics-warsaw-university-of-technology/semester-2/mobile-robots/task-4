function [forwBackVel, leftRightVel, rotVel, finish] = ...
    solution4(pts, contacts, ~, ~,varargin)
% The control loop callback function - the solution for Task 
    
    % declare the persistent variable that keeps 
    % the state of the Finite
    % State Machine (FSM)
    persistent state;
    persistent initialmove;

    if length(varargin) ~= 1,
        error(['Wrong number of additional ' ...
            'arguments: %d\n'], length(varargin));
    end 
    dist_wall = varargin{1};

    if isempty(state)
        % the initial state of the FSM is 'init'
        state = 'init';
    end

    % initialize the robot control variables 
    % (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

    % TODO: manage the states of FSM
    % Write your code here...
    u_rad_max = 4;
    u_tan_max = 8;
    u_rot_max = 4;
    gain_P = 100;

    % process the LIDAR sensor data
    % get the laser contact points in sensor's coordinates
    points = [ pts(1,contacts)' pts(2,contacts)' ];
    % calculate the distances
    distances = (pts(1,contacts)'.^2 + pts(2,contacts)'.^2).^0.5;
    % get the closest point
    [min_value, min_index] = min(distances(:));

    if strcmp(state, 'init')
        state = 'move';
        fprintf('changing FSM state to %s\n', state);
    elseif strcmp(state, 'move')
        min_dist_point = (points(min_index,:))';
        % calculate the angle to the closest point
        closest_angle = atan2(points(min_index,2),points(min_index,1));
    
        error_dist = min_value - dist_wall;
        error_theta = angdiff(closest_angle,-pi/2);
        
        rad_dir = min_dist_point/norm(min_dist_point);
        tan_dir = Rotation_matrix(pi/2)*rad_dir;

        u_r = gain_P * error_dist;
        if u_r > u_rad_max
            u_r = u_rad_max;
        elseif u_r < -u_rad_max
            u_r = -u_rad_max;
        end

        u_t = u_tan_max;

        u_rot = gain_P *error_theta;
        if u_rot > u_rot_max
            u_rot = u_rot_max;
        elseif u_rot < -u_rot_max
            u_rot = -u_rot_max;
        end

        if abs(error_dist) > 0.005 && isempty(initialmove)
            Vel = rad_dir * u_r;
        else
            initialmove="false";
            Vel = rad_dir * u_r + tan_dir * u_t;
        end
        
        forwBackVel = Vel(2);
        leftRightVel = Vel(1);
        rotVel = u_rot;
    else
        error('Unknown state %s.\n', state);
    end
end

function R = Rotation_matrix(theta)
    R = [cos(theta), -sin(theta); 
        sin(theta), cos(theta)];
end